import serial
import argparse

parser = argparse.ArgumentParser(description='Send an image via UART')
parser.add_argument('-t',  '--tty', default = '/dev/ttyUSB1', type = str, help= 'Path to the tty device')
parser.add_argument('-b',  '--baudrate', default = '115200', type = int, help ='Baudrate')
parser.add_argument('image', type = str, help ='Path to the image to send')
parser.add_argument('-d',  '--debug', help="Debug mode", action="store_true")
args = parser.parse_args()

def printErr (*args):
  import builtins
  builtins.print (*args)
  
if not args.debug:
  def print(*args):
    pass


H=32
W=32
CH=3

ser = serial.Serial(args.tty, args.baudrate)

f = open(args.image, "rb")
lines = int.from_bytes(f.read(2), byteorder='big')


if lines != 0x5036: ## 0x50 ASCII for 'P' and 0x36 ASCII for 6

  printErr ("[ERROR] Only PPM images are supported!")
  exit(1)
  
print ("Image: {}".format(args.image) )

header_lines=3
while header_lines>0:
  b = f.read(1)
#  print (b)
  if int.from_bytes(b, byteorder='big')==0x0A:
    header_lines -= 1
    

print (ser.readline()) # waits for "Started" string

if args.debug:
  ser.write(str.encode("d\n"))
  print (ser.readline())
  
s=0
for i in range (H*W*CH):
  b = f.read(1)
  print ("\n\n\n\n####### Sending #######")
  print (b)
  print ("#######################")
  print (i)
  ser.write(b)
  if args.debug:
    resp=ser.readline()
    print (resp)
  s+=int.from_bytes(b, byteorder='big')

printErr (ser.readline())
printErr ("checksum: ", s)

f.close()
  
print ("Receiving response...")
header_line=0
pixels=0
f = open("./received.ppm", "w")
while (True):
  b = ser.read(1)
  if header_line <3: #receiving header
    if b==10:
      header_line +=1
    
  else: # reciving pixels
    pixels += 1
  f.write (b)
  
  if pixels >= H*W*CH:
    break
  
f.close()
print ("DONE! Check the file received.ppm in your working dir")


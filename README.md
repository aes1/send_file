# Use a terminal to send the PPM file
We are going to use Realterm to send the PPM file over the UART. Realterm is only available for Windows.  
The Linux users can do the same thing using the [script](send_image_bin.py) we provvided you with.

Before sending the image we need to have a server listening on the other side. The server can be any applycation able to read the characters from the UART. 


**Linux**  
On Linux simply lauch the script. It will wait for the server on the board. Once it is up the script starts sending the image.  
```
python3 send_image_bin.py image.ppm -d
```
The script is based on the library [pyserial](https://pythonhosted.org/pyserial/index.html)
You can change the UART teminal passing the parameter `-t path to the device` and you can use a different BAUDRATE using the parameter `-b BAUDRATE`.
the `-d` argument enables the debug mode.

For example:  
```
python3 send_image.py horse.ppm -t /dev/ttyUSB1 -b 115200 -d
```


To find the device you ZYBO board is mapped to you can use `dmesg`  
```
dmesg | grep tty
```
![dmesg](dmesg.png)  
the last line should report the name of the last UART device connected.

You can use the checksum to verify if everything works fine.

**Windows**  
On Windows you have to follow some steps:  
Open realterm and connect to the COM port (x=\VCP0, x can be any number). Click open to connect.  
![realterm](realterm/realterm.png)  

Start the application on SDK, the string `Hello world` (or any other string) should appear in the terminal.  
![started](realterm/started.png)

Open the tab `Send`, choose the PPM file and click send. A lot of prints, one for each line of the file should be printed.  
![send](realterm/send.png)

The terminal will show the output of your server running in the board.

# Receive the file
The server can send back a file after the processing.

**Linux**
On Linux you can use the same script. The scripts waits for an answare from the server and saves the output as a file.

**Windows**
On Realterm open the tab `Capture`. Select a filename and click on Start: Overwrite.  
From now on all the characters printed in the termilal are stored in the file.

